# Educate-In-Place #



### What is Educate-In-Place? ###

**Educate-In-Place (E-I-P)** is a project that promotes literacy by providing a library management system 
for physical books as well as pdf books, lessons, tutorials, and informative pictograms via low cost display devices.

**E-I-P** will enhance and supplant the **African Library Project** book drives by providing books in PDF format.

Participants will not only be encouraged to read, but also learn valuable computer skills 
while operating a tablet or smart phone.